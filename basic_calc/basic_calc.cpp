#include <iostream>
#define FALSE 0 //(int)
#define TRUE 1  //(int)
#define NOT_ALLOWED  8200

void print_exeption()
{
	std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
}

int add(int& a, int b) 
{
	if (a == NOT_ALLOWED || b == NOT_ALLOWED)
	{
		print_exeption();
		return FALSE;
	}
	if (a + b == NOT_ALLOWED)
	{
		print_exeption();
		return FALSE;
	}
	a = a + b;
	return TRUE;
}

int  multiply(int& a, int b) 
{
	int sum = 0;
	int& Rsum = sum;

	if (a == NOT_ALLOWED || b == NOT_ALLOWED)
	{
		print_exeption();
		return FALSE;
	}

	for (int i = 0; i < b; i++)
	{
		if (sum == NOT_ALLOWED || !add(sum, a))
		{
			return FALSE;
		}
	}
	a = Rsum;
	return TRUE;
}

int  pow(int & a, int b)
{
	if (a == NOT_ALLOWED || b == NOT_ALLOWED)
	{
		print_exeption();
		return FALSE;
	}

	int exponent = 1;
	for (int i = 0; i < b; i++) 
	{
		if (!multiply(exponent, a))
		{
			print_exeption();
			return FALSE;
		}
	}
	int & Rexponent = exponent;
	a = Rexponent;
	return exponent;
}

int main(void)
{
	int ansAdd = 0;
	int ansMult = 0;
	int ansPow = 0;
	int a = 8100;
	int b = 100;
	int & Ra = a;


	ansAdd = add(Ra, b);
	if (ansAdd)
	{
		std::cout << a << std::endl;
	}
	a = 820;
	b = 10;
	ansMult = multiply(Ra, b);
	if (ansMult)
	{
		std::cout << a << std::endl;
	}
	a = 3;
	b = 4;
	ansPow = pow(Ra, b);
	if (ansPow)
	{
		std::cout << a << std::endl;
	}
	a = 8200;
	b = 1;
	ansPow = pow(Ra, b);
	if (ansPow)
	{
		std::cout << a << std::endl;
	}
	system("pause");
}