#include "MathUtils.h"


double MathUtils::CalPentagonArea(double a)
{
	return 1.72048*a*a;
}

double MathUtils::CalHexagonArea(double a)
{
	return 2.59808*a*a;
}
