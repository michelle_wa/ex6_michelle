#include "parallelogram.h"
#include "shape.h"
#include "Pentagon.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>
#include "MathUtils.h"

Pentagon::Pentagon(std::string col, std::string nam, int height) :Shape(col, nam)
{
	setHeight(height);
}

void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Height is " << getHeight() << std::endl
		<< "Area is " << MathUtils::CalPentagonArea(getHeight()) << std::endl;
}

void Pentagon::setHeight(double height1)
{
	if (height1 < 0)
	{
		throw shapeException();
	}
	else
	{
		height = height1;
	}
}

double Pentagon::getHeight()
{
	return height;
}

double Pentagon::CalPerimater()
{
	return 5 * height;
}

double Pentagon::getCalPerimater()
{
	return 5 * height;
}

double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(getHeight());
}

