#include "parallelogram.h"
#include "shape.h"
#include "Pentagon.h"
#include "circle.h"
#include "quadrilateral.h"
#include "hexagon.h"
#include "shapeException.h"
#include <iostream>
#include "MathUtils.h"

hexagon::hexagon(std::string col, std::string nam, int height) :Shape(col, nam)
{
	setHeight(height);
}

void hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Height is " << getHeight() << std::endl
		<< "Area is " << MathUtils::CalHexagonArea(getHeight()) << std::endl;
}

void hexagon::setHeight(double height1)
{
	if (height1 < 0)
	{
		throw shapeException();
	}
	else
	{
		height = height1;
	}
}

double hexagon::getHeight()
{
	return height;
}

double hexagon::CalPerimater()
{
	return 6 * height;
}

double hexagon::getCalPerimater()
{
	return 6 * height;
}

double hexagon::CalArea()
{
	return MathUtils::CalPentagonArea(getHeight()); //static func
}

