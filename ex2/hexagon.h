#pragma once
#include "shape.h"

class hexagon : public Shape
{
public:
	void draw();
	double CalArea(); //returns the area
	hexagon(std::string, std::string, int rib);
	double CalPerimater();
	double getCalPerimater();

	double getHeight();
	void setHeight(double height1);

private:
	double height;
};
