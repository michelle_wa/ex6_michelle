#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "Pentagon.h"
#include "hexagon.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"

int main()
{
	std::string nam, col;
	double rad = 0, ang = 0, ang2 = 180;
	int height = 0, width = 0;

	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, height);
	hexagon hexa(nam, col, height);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent;
	Shape *ptrhexa = &hexa;

	char shapetype;
	std::string shapes;
	char x = 'y';
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 'P',hexagon = 'h';


	std::cout << "Enter information for your objects" << std::endl;
	while (x != 'x') 
	{
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, P = pentagon, h = hexagon" << std::endl;
		std::cin >> shapes;
		if (strlen(&shapes[0]) > 1)
		{
			shapetype = 'F'; //Fail - More letters than necessary
		}
		else
		{
			shapetype = shapes[0]; 
		}
		try
		{
			switch (shapetype) 
			{
			case 'c':  //circle
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (std::cin.fail())
				{
					throw InputExeption();
				}
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':  //quadrilateral
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					throw InputExeption();
				}
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':  //rectangle
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					throw InputExeption();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':  //parallelogram
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail())
				{
					throw InputExeption();
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'P':  //pentagon
				std::cout << "enter color, name,  height for pentagon" << std::endl;
				std::cin >> col >> nam >> height;
				if (std::cin.fail())
				{
					throw InputExeption();
				}
				pent.setColor(col);
				pent.setName(nam);
				pent.setHeight(height);
				ptrpent->draw();
				break;
			case 'h':  //hexagon
				std::cout << "enter color, name,  height for hexagon" << std::endl;
				std::cin >> col >> nam >> height;
				if (std::cin.fail())
				{
					throw InputExeption();
				}
				hexa.setColor(col);
				hexa.setName(nam);
				hexa.setHeight(height);
				ptrhexa->draw();
				break;

			case 'F':
				std::cout << "Warning - Don't try to build more than one shape at once \n" << std::endl;
				break;

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		catch (std::exception & e)  //Input Exception + shape exception
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}


	system("pause");
	return 0;

}