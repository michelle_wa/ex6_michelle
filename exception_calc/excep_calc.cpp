#include <iostream>
#define NOT_ALLOWED  8200


int add(int a, int b) 
{
	if (a == NOT_ALLOWED || b == NOT_ALLOWED || a+b == NOT_ALLOWED)
	{
		throw(NOT_ALLOWED);
	}
	return a + b;
}

int  multiply(int a, int b) 
{
	int sum = 0;
	for (int i = 0; i < b; i++) 
	{
		sum = add(sum, a);
	}
	return sum;
}

int  pow(int a, int b) 
{
	int exponent = 1;
	for (int i = 0; i < b; i++) 
	{
		exponent = multiply(exponent, a);
	}
	return exponent;
}

int main(void) 
{
	try
	{
		std::cout << pow(5, 5) << std::endl;
		std::cout << pow(8200, 1) << std::endl;
	}
	catch (const int not_allowed)
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	system("pause");

}